# sisop-praktikum-modul-2-2023-WS-F06



## Soal Nomer 1

A. Mendownload file dari https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq dan melakukan unzip terhadap file tersebut

Pada code di soal ini menggunakan fungsi fork() untuk membuat 2 buah child process yang berbeda. 

Child process yang pertama digunakan untuk mendownload file zip dari URL yang ada.

```
execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);

```
Pada proses ini digunakan 'execlp' untuk menjalankan perintah 'wget' yang digunakan untuk mendownload file zip dari URL yang ada.
Pada perintah 'wget' diberi argumen -o untuk menyimpan hasil download ke dalam file bernama binatang.zip

Setelah Child process yang pertama selesai, parent process akan menunggu child process tadi selesai menggunakan fungsi 'waitpid()'

```
waitpid(pid, &status, 0);

```
Jika child process pertama sudah selesai, makan parent process akan membuat child process yang kedua menggunakan 'fork()'.

```
execlp("unzip", "unzip", "-q", "binatang.zip", "-d", "binatang_folder", NULL);
```
Pada child process yang kedua digunakan execlp kembali untuk menjalankan perintah 'unzip' yang digunakan untuk mengekstrak file zip kedalam folder bernama 'binatang_folder' menggunakan argumen -d. Argumen -q juga digunakan untuk menghilangkan output dari perintah 'unzip'.

Setelah child process kedua selesai, parent process akan menunggu child process tersebut selesai menggunakan fungsi waitpid(). 

Jika terjadi kesalahan dalam membuat child process atau menjalankan perintah wget atau unzip, maka program akan keluar.

B. Melakukan pemilihan acak dari file gambar yang ada untuk dilakukan shift penjagaan oleh Grape-kun

```
if ((dir = opendir("binatang_folder")) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            ext = strrchr(entry->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                count++;
            }
        }
```
Langkah awal membuka direktori 'binatang_folder' yang berisi file gambar hewan adalah menggunakan fungsi 'opendir'. Jika direktori berhasil dibuka, maka fungsi akan mulai melakukan penghitungan jumlah file gambar yang ada didalamnya menggunakan fungsi loop yaitu 'readdir'. 

Fungsi 'readdir' akan membaca setiap entry(file) yang ada pada direktori dan mengembalikan pointer ke struktur 'dirent'.

Setiap entry dicek apakah merupakan file gambar dengan menggunakan fungsi 'strrchr()' yang akan mengembalikan pointer ke karakter terakhir yang sama dengan karakter yang dicari (dalam hal ini titik) dalam string. 

Kemudian akan dibandingkan dengan string ".jpg", ".jpeg", atau ".png" menggunakan fungsi strcmp(). Jika benar, maka variabel count yang digunakan untuk menghitung jumlah file gambar akan bertambah 1.

```
closedir(dir);
dir = opendir("binatang_folder");
```
Setelah selesai menghitung jumlah file gambar, fungsi menutup direktori dengan menggunakan fungsi closedir() dan membuka kembali direktori tersebut untuk memilih file gambar acak.

```
srand(time(NULL));
        int r = rand() % count + 1;
        count = 0;
        while ((entry = readdir(dir)) != NULL) {
            ext = strrchr(entry->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                count++;
                if (count == r) {
                    sprintf(path, "%s", entry->d_name);
                    printf("Grape-kun melakukan shift penjagaan pada hewan %s\n", path);
                    break;
                }
            }
        }
```

Kemudian, fungsi memanggil fungsi srand() untuk mengatur seed dari fungsi rand() agar menghasilkan angka acak yang berbeda pada setiap pemanggilan.

Nilai 'seed' ini mempengaruhi nilai acak dari fungsi rand(), jadi ketika menerapkan ini bisa saja kita menghasilkan output acak yang sama.

Fungsi rand() sendiri digunakan untuk memilih angka acak antara 1 dan count (Sesuai jumlah perhitungan dari count diatas).

Lalu dilakukan loop lagi menggunakan fungsi readdir(), fungsi ini akan memeriksa setiap entry yang ada dan menghitung file gambar yang ditemukan. Ketika jumlah file gambar yang ditemukan mencapai angka acak yang dihasilkan sebelumnya, maka nama file tersebut disimpan dalam string path menggunakan fungsi sprintf(). 

Kemudian fungsi printf() akan menampilkan kalimat yang tertera dan nama file acak yang terpilih.

```
closedir(dir);
    } else {
        perror("Gagal");
```

Terakhir, fungsi menutup direktori dengan fungsi closedir(). Jika direktori tidak berhasil dibuka, maka fungsi akan menampilkan pesan 'Gagal' menggunakan fungsi perror().

C. Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir, lalu memfilter file gambar hewan dan dimasukkan ke direktori yang sesuai dengan tempat tinggalnya.

Pada fungsi ini akan membuat 3 direktori yaitu HewanDarat, HewanAmphibi, dan HewanAir menggunakan perintah 'mkdir'. -p pada perintah 'mkdir' digunakan jika sudah terdapat direktori tersebut, maka tidak perlu untuk membuat lagi.

```
system("mkdir -p HewanDarat HewanAmphibi HewanAir");
```
Pertama untuk membuka direktori "binatang_folder" kita menggunakan opendir(). Kemudian dilakukan looping sebanyak jumlah file dalam direktori dengan readdir().

```
if ((dir = opendir("binatang_folder")) != NULL) {
        while ((entry = readdir(dir)) != NULL)
```
Berikut adalah tahapan dari looping yang dilakukan 

```
if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                if (strstr(entry->d_name, ".jpg") != NULL || strstr(entry->d_name, ".jpeg") != NULL || strstr(entry->d_name, ".png") != NULL) {
                    // yang mengandung kata darat
                    if (strstr(entry->d_name, "darat") != NULL) {
                        sprintf(tempattinggal, "HewanDarat/");
                    // yang mengandung kata amphibi
                    } else if (strstr(entry->d_name, "amphibi") != NULL) {
                        sprintf(tempattinggal, "HewanAmphibi/");
                    // yang mengandung kata air
                    } else if (strstr(entry->d_name, "air") != NULL) {
                        sprintf(tempattinggal, "HewanAir/");
                    } else {
                        continue;
                    }
```
1. Pada tahap ini file dicek apakah merupakan file "." atau file "..", jika ternyata iya maka lewati file tersebut dengan 'continue'.
```
if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
```
2. Kemudian file dicek menggunakan fungsi 'strstr()' untuk melihat apakah file memiliki ekstensi '.jpg', '.jpeg', atau '.png'. Jika bukan maka lewati file tersebut dengan 'continue'.

```
if (strstr(entry->d_name, ".jpg") != NULL || strstr(entry->d_name, ".jpeg") != NULL || strstr(entry->d_name, ".png") != NULL)
```
3. Jika file tersebut adalah file gambar, maka akan dicek jenis binatang yang terdapat pada nama file. Fungsi strstr() digunakan untuk mencari apakah file tersebut mengandung kata "darat", "amphibi", atau "air". Jika ada, variabel tempattinggal akan diisi dengan nama folder yang sesuai (yaitu "HewanDarat", "HewanAmphibi", atau "HewanAir"). Jika tidak ada, file tersebut akan dilewati dengan continue.

```
if (strstr(entry->d_name, "darat") != NULL) {
                        sprintf(tempattinggal, "HewanDarat/");
                    // yang mengandung kata amphibi
                    } else if (strstr(entry->d_name, "amphibi") != NULL) {
                        sprintf(tempattinggal, "HewanAmphibi/");
                    // yang mengandung kata air
                    } else if (strstr(entry->d_name, "air") != NULL) {
                        sprintf(tempattinggal, "HewanAir/");
                    } else {
                        continue;
```
4. Terakhir adalah membuat string 'cmd' dimana berisi perintah 'mv' untuk memindahkan file dari direktori 'binatang_folder' ke direktori yang sesuai. Dan perintah system() digunakan untuk menjalankan perintah dari 'mv'.

```
sprintf(cmd, "mv \"binatang_folder/%s\" \"%s%s\"", entry->d_name, tempattinggal, entry->d_name);
                    system(cmd);
```
Terakhir adalah menutup direktori dengan fungsi closedir(), jika direktori ternyata tidak berhasil dibuka maka akan menampilkan pesan 'Gagal' menggunakan fungsi perror().

D. Melakukan zip terhadap folder HewanDarat, HewanAmphibi, dan HewanAir agar menghemat penyimpanan

1. Mendeklarasikan variabel pid dan status, dimana pid berguna untuk menyimpan pid dari child process yang dihasilkan, dan status digunakan untuk menyimpan status dari child process yang selesai
```
pid_t pid;
    int status;
```
2. Mendeklarasi array of strings 'folders' yang berisi tiga nama folder yang akan di-zip dan variabel 'num_folders' yang menyimpan jumlah folder/banyak folder di dalam array tersebut.
```
char* folders[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    int num_folders = 3;
```
3. Pada bagian ini, dilakukan loop sebanyak jumlah folder yang ada pada array 'folders'. Setiap kali melakukan loop, program akan membuat child process menggunakan fungsi fork().
```
for (int i = 0; i < num_folders; i++) {
        pid = fork();
}
```
4. Jika pid yang dihasilkan dari fork() bernilai 0, maka program berada dalam child process. Di dalam child process program akan membuat nama file zip dengan format nama_folder.zip menggunakan fungsi sprintf(). Selanjutnya, program akan mengeksekusi zip menggunakan fungsi execlp(). -r digunakan untuk membuat file zip yang berisi seluruh file dan folder yang ada di dalam folder 'folders[i]'.
```
if (pid == 0) { // Child process
            char filename[100];
            sprintf(filename, "%s.zip", folders[i]);
            execlp("zip", "zip", "-r", filename, folders[i], NULL);
}
```
5. Jika pid yang dihasilkan dari fork() lebih besar dari 0, maka program berada dalam parent process. Di dalam parent process ini, program akan menunggu child process selesai menggunakan fungsi waitpid().
```
else if (pid > 0) { // Parent process
    waitpid(pid, &status, 0); // Menunggu child process selesai
}
```
Namun jika ternyata pid yang dihasilkan negatif maka akan menampilkan pesan 'Error'

***
Pada fungsi main akan dilakukan pemanggilan secara urut dari fungsi downloadUnzip(), randomImage(), makeDirectories(), move().

Lalu setelahnya akan dilakukan fungsi untuk melakukan zip folder.

Dan terakhir dilakukan penghapusan semua folder lain yang tidak di zip menggunakan perintah 'rm'.
***

## Soal Nomer 2
a. buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]

```
time_t cur_time = time(NULL);
    struct tm* tm = localtime(&cur_time);
    strftime(dir, sizeof(dir), "%Y-%m-%d_%H:%M:%S", tm);
```
- Menggunakan time_t dan memakai localtime(), untuk mengambil waktu sekarang.
- Kemudian di strftime untuk menjadikan waktu sekarang sebagai string nama folder dengan format "%Y-%m-%d_%H:%M:%S".

```
char path[] = "/home/plankton/soalnomor2/";
	strcat(path, dir);

	int child_id = fork();
        if (child_id == -1) {
            perror("fork");
           exit(EXIT_FAILURE);
        }

       if (child_id == 0) {
	    char* argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
        }
        sleep(30);
    }
```
- Inisialisasi path[], path menuju dimana folder ingin dibuat
- strcat(path, dir), Menggabungkan path dengan nama folder yang sesuai waktu tadi.
- Menggunakan fork lagi agar proses execv dapat berjalan terus menerus.
- execv mkdir untuk membuat directory.
- Terakhir, sleep(30) untuk membuat direktorinya setiap 30 detik
## Soal Nomer 3
Soal Nomor 3 adalah membuat program untuk mendownload file zip dan unzip dengan menghapus file yang tidak dibutuhkan dan megambil pemain terbaik dari setiap posisi pada sepak bola dengan ketentuan kiper hanya terdiri dari 1 orang saja.

***
char link[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"; \
	pid_t pid = fork();\
	if (pid == 0){\
		execlp("wget", "wget", "-O", "players.zip", link, NULL);\
		exit(1);\
	}\
	int status;\
	wait(&status);

***
* char link[] digunakan untuk menyimpan url download
* pid_t yaitu signed integer yang merepresentasikan PID(process id) dan akan merepresentasikan fork()
* fork() merupakan cara untuk membuat child process sehingga dapat mengeksekusi program dan salahsatunya adalah exec
* execlp merupakan pengembangandari execl, pada execl kita akan menggunakan path program sebagai argumen pertama, sedangkan execlp akan menggunakan nama program sebagai argumen pertama sebagai path untuk mencapai direktori tempat variabel disimpan
* execlp("wget", "wget", "-O", "players.zip", link, NULL);\
-pada wget pertama, yaitu akan merepresentasikan nama dalam bentuk string sebagai path menuju direktori untuk mengakses wget \
-wget yang kedua adalah sebuah command untuk mengeksekusi wget \
-Pada wget kita akan mendownload file dalam link dengan nama yang sesuai pada link, sehingga untuk memanipulasi agar file yang didownload akan didownload dengan nama yang kita inginkan, maka -O digunakan agar file yang didownload akan sesuai dengan nama setelah -0 \
-kemudian setelah nama dari file yang diinginkan, maka kita akan memasukkan link mengunduh file dan menandai akhir proses execlp dengan NULL. 
* exit(1) digunakan untuk mengembalikan nilai 1 ketika error terjadi pada suatu proses sebagai penanda error
* status digunakan sebagai flag, sehingga pada wait(&status) akan menunggu child process untuk selesai hingga status bernilai benar.
***
execlp("unzip", "unzip", "-q", "players.zip", NULL);
***
* unzip -q digunakan untuk unzip file zip dan tidak akan mengembalikan apapun dikarenakan -q atau quiet tidak akan mengembalikan apapun apabila benar dan akan mengembalikan false apabila salah
***
execlp("find", "find", "./players", "-type", "f", "!", "-name", "\*ManUtd\*", "-delete", NULL);
***
* find digunakan untuk mencari dalam suatu direktori tertentu, yaitu pada ./players
* type digunakan untuk menspesifikkan file apa yang kita cari,bisa direktori maupun file, sehingga untuk menspesifikasikan bahwa kita hanya ingin mencari file dalam direktori ./players maka -f yaitu file digunakan
* ! -name digunakan untuk mencari nama yang tidak sesuai dengan nama file yang ingin kita cari, yaitu ManUtd.
* asteris pada sebelum dan sesudah ManUtd adalah untuk mengindikasikan bahwa terdapat tulisan lain setelah dan sebelum, sehingga apabila dalam nama tersebut mengandung kata ManUtd maka akan terdeteksi \
-proses delete dilakukan dengan menggunakan -delete yang disediakan pada command find
***
execlp("rm", "rm", "players.zip", NULL);
***
* rm atau remove digunakan untuk mendelete, dalam hal ini adalah file players.zip
***
execlp("mkdir", "mkdir", "./players/Kiper", "./players/Bek", "./players/Gelandang", "./players/Penyerang", NULL);
***
* mkdir digunakan untuk membuat direktori
* execlp dapat mengeksekusi beberapa argumen dengan menggunakan command yang digunakan pada argumen 2, sehingga selama nilai tidak null maka execlp akan terus mkdir dengan ketentuan ./players/{Kiper, Bek, Gelandang, dan Penyerang}
***
1. execlp("find", "find", "./players", "-name", "*Kiper*", "-type", "f", "-exec", "mv", "{}", "./players/Kiper", ";", NULL);\
2. execlp("find", "find", "./players", "-name", "*Bek*", "-type", "f", "-exec", "mv", "{}", "./players/Bek", ";", NULL);\
3. execlp("find", "find", "./players", "-name", "*Gelandang*", "-type", "f", "-exec", "mv", "{}", "./players/Gelandang", ";", NULL);\
4. execlp("find", "find", "./players", "-name", "*Penyerang*", "-type", "f", "-exec", "mv", "{}", "./players/Penyerang", ";", NULL);

***
* find digunakan untuk mencari setiap file(-type f) dalam direktori ./players yang namanya mengandung kata Kiper.
* -exec digunakan agar exec dapat mengeksekusi command yang lain, dalam hal ini adalah mv.
* {} merepresentasikan file-file yang memenuhi command pertama, dan akan memindahkan(mv atau move) file-file tersebut ke direktori yang baru, yaitu ./Players/Kiper
* semicolon digunakan agar -exec dapat dieksekusi oleh program

***
int bek, gelandang, penyerang;\
	int sum=1;

	printf("\n\ninput banyak pemain bek yang diinginkan: ");
	scanf("%d", &bek);
	sum += bek;
	printf("%d\n",sum);

	printf("input banyak pemain gelandang yang diinginkan: ");
	scanf("%d", &gelandang);
	sum += gelandang;
	printf("%d\n",sum);

	printf("input banyak pemain penyerang yang diinginkan: ");
	scanf("%d", &penyerang);
	sum += penyerang;
	printf("%d\n",sum);
***
* Program untuk melakukan input jumlah bek, gelandangm, dan penyerang yang diinginkan untuk membuat satu tim bola
* sum=1 dikarenakan terdapat ketentuan bahwa kiper hanya boleh satu
***
if(sum == 11)\
		buatTim(bek,gelandang,penyerang);\
	else\
		printf("Jumlah tim kurang dari standard(11 pemain)\n");
***
* jika jumlah pemain yang diinput adalah 11 maka kita dapat melanjutkan kedalam proses pembuatan tim dan passing nilai bek, gelandang, dan penyerang kedalam fungsi buatTim
***
char command[100];\
                sprintf(command, "echo \"Kiper:\" >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);\
		execlp("sh", "sh", "-c", command, NULL);\
		exit(1);


char command[200];\
		sprintf(command,"touch Formasi_%d_%d_%d.txt && ls ./players/Kiper | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang, bek, gelandang, penyerang);\
		execlp("sh", "sh", "-c", command, NULL);\
		exit(1);

char command[100];\
                sprintf(command, "ls -1v ./players/Bek | sort -t _ -k 4n | tail -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);\
                execlp("sh", "sh", "-c", command, NULL);\
                exit(1);
***
* command digunakan untuk menyimpan printf pada sprintf
* 100 digunakan agar apabila return dari nilai sprintf dibawah atau sama dengan 100, dapat tersimpan kedalam command
* command menyimpan perintah print Kiper: kedalam file formasi_jumlah bek_jumlah gelandang_jumlah penyerang
* execlp("sh", "sh", "-c", command, NULL); \
-sh atau shell akan menajalankan program secara interaktif hingga input dimasukkan, sehingga -c diperlukan sebagai input agar command dapat dieksekusi menggunakan shell
* pada berikutnya, command[200] dikarenakan sprintf mengembalikan nilai hingga 173 byte, sehingga perlu diperbesar hingga 200 untuk berjaga-jaga return nilai melebihi 173
* touch digunakan untuk dapat membuat file apabila file tersebut belum tersedia untuk argumen berikutnya dijalankan
* ls digunakan untuk list nama file dalam direktori tersebut, dikarenakan kiper dengan rating paling tinggi adalah file pertama, maka head -n 1 akan mengambil file paling atas untuk dimasukkan kedalam ile formasi_jumlah
* ls -1v digunakan karena -1v akan membuat list berdasarkan angka yang terdapat pada nama file-file didalam direktori
* sort -t digunakan untuk menspesifikasikan adanya _ sebagai separator dalam nama sehingga kita akan tetap melakukan sort berdasarkan value yang dipisahkan oleh d_
* -k untuk menspesifikasikan key yang digunakan dalam sorting dalam hal ini adalah field ke empat dalam nama yang dipisahkan dengan -t _. Dalam file zip, angka rating diindikasikan pada field keempat
* tail -n %d  
-digunakan untuk mengambil file paling bawah dimulai dari %d, yaitu %d dari bawah, hal ini dikarenakan pada sorting, file akan diurutkan secara ascending atau dari terkecil hingga terbesar
## Soal Nomer 4
Soal Nomer 4 adalah membuat program menjalankan script bash yang menyerupai crontab.

```
 if (argc < 4) {
        printf("./program (schedule) (/path/ke/sh)\n");
        printf("Contoh: \"./mainan \\* 44 5 /home/plankton/shell.sh\"\n");
    }
```
Karena menyerupai crontab, maka untuk menjalankan program ini harus menggunakan 4 argumen. Contohnya:
> ./mainan \* 44 5 /home/plankton/shell.sh

- Dimana "./mainan" adalah untuk mengeksekusi program, "\*" adalah argumen ke 1 untuk jam, "44" adalah argumen ke 2 untuk menit, dan "5" adalah argumen ke 3 untuk detik, dan "/home/plankton/shell.sh" adalah argumen ke 4 untuk path shell script yang ingin dijalankan.

- Jika argumen < 4 yang berarti argumen tidak lengkap, maka akan ditampilkan pesan error untuk contoh yang benar.

```
    char *path_sh = argv[4];
    char *input_jam = argv[1];
    char *input_menit = argv[2];
    char *input_detik = argv[3];
```

Argumen-argumen yang sudah diinput akan dimasukkan ke dalam variabel-variabel. Contoh: argumen ke 4 yang merupakan path file shell script akan di masukkan ke variable path_sh.
***

2. Untuk mengelola jam, menit, dan detik yang dimasukkan user agar menjadi seperti crontab. Maka, input jam, menit, detik yang berupa string akan diubah menjadi integer.

    Untuk proses perubahan jam/menit/detik itu sama. Sehingga, hanya diambil salah satu proses untuk dijadikan contoh. Contohnya proses menit:

```
 if (strcmp(input_menit, "*") == 0) {
        menit = -1;
    } else {
        menit = atoi(input_menit);
        if (menit < 0 || menit > 59) {
            printf("Input Menit tidak valid! (60 < Menit >= 0)\n");
            exit(1);
        }
    }
```
- Jika input argumen ke 2 user atau input menitnya adalah *. Maka, variable menit akan diubah menjadi -1. Mengapa -1? karena menit mulai dari 0 sampai 60, daripada mengambil angka yang lebih dari 60, lebih simple mengambil -1.

- Jika tidak * atau jika inputannya angka, maka inputan menit akan diubah menjadi integer dan akan dimasukkan ke variable menit. "atoi()" berfungsi untuk mengubah string menjadi integer.

- Setelah diubah, akan dicek apakah angka menitnya valid. Jika menitnya lebih dari 59 atau kurang dari 0, maka akan ditampilkan pesan error bahwa input menit tidak valid dan akan di exit dari program.

***
3. Karena proses ini dijalankan di background. Jadi disini dipakai daemon proses untuk menjalankan program di background. Untuk daemon proses, disini memakai template daemon proses dari modul 2. Jadi proses dalam while loop saja yang akan dijelaskan.

```
while (1) {
        time_t waktu_now;
        struct tm *info;

        time(&waktu_now);
        info = localtime(&waktu_now);

        if ((menit == -1 || menit == info->tm_min)
            &&(jam == -1 || jam == info->tm_hour)
            && (detik == -1 || detik == info->tm_sec)) {

            	pid_t pid2 = fork();

    	        if (pid2 == -1) {
       	            exit(EXIT_FAILURE);
    	        } 
                else if (pid2 == 0) {
	                umask(0);
                    char *args[] = {"/bin/sh", path_sh, NULL};
                    execv(args[0], args);
                }
	    }
        sleep(1);
	}

```
Bagian pertama, untuk mengambil waktu sekarang / current time, digunakan syntax seperti berikut :
```
time_t waktu_now;
struct tm *info;

time(&waktu_now);
info = localtime(&waktu_now);

```
- Digunakan "time_t" untuk mengambil time setelah Epoch, dan diberi nama waktu_now. 
- Kemudian "struct tm *info" untuk strukturnya, Contohnya : "tm_sec" untuk seconds, "tm_min" untuk menit, dll. Kemudian  structnya diberi nama info.
- "time(&waktu_now)" untuk assign waktunya ke variable waktu_now.
- Terakhir, "localtime(&waktu_now)" untuk mengambil waktu sekarang dan assign ke struct info.

Bagian kedua adalah untuk mengetahui apakah inputan user sudah sesuai dengan waktu sekarang atau belum.
```
if ((menit == -1 || menit == info->tm_min)
            &&(jam == -1 || jam == info->tm_hour)
            && (detik == -1 || detik == info->tm_sec))
```
- Untuk mengeksekusi shell script seperti crontab, maka waktu sekarang harus sama dengan yang diinputkan user.
- Jadi, jika user input '*' atau input user sama dengan waktu sekarang, maka akan lanjut ke eksekusi file shell script.
- Contoh: Jika menit == -1, yang artinya menitnya itu '*', atau jika menit == info->tm_min, yang artinya menitnya sama dengan menit waktu sekarang, dan jam & detik nya juga sesuai. maka akan dilanjut ke eksekusi.

Bagian ketiga adalah eksekusi shell script.
```
pid_t pid2 = fork();

if (pid2 < 0) {
    exit(EXIT_FAILURE);
} 
else if (pid2 == 0) {
    char *args[] = {"/bin/sh", path_sh, NULL};
    execv(args[0], args);
}
```
- Dikarenakan jika menggunakan execv() hanya bisa mengeksekusi filenya sekali, maka diperlukan fork exec.
- Menyimpan PID ke pid2.
- Jika pid2 < 0, artinya keluar saat fork gagal.
- Jika pid2 == 0, maka akan mengeksekusi execv.
- execv akan menjalankan shell script dari path yang sudah dimasukkan oleh user

> sleep(1);

Terakhir, sleep(1) untuk menghindari CPU overload.

## Revisi Nomor 4
Dikarenakan error message yang muncul saat mengeksekusi file mainan ini dengan argumen tidak tepat, sehingga harus diperbaiki.
```
if (argc < 5 || argc > 5) {
        printf("./program (schedule) (/path/ke/sh)\n");
        printf("Contoh: \"./mainan \\* 44 5 /home/plankton/shell.sh\"\n");
        exit(1);
    }
```
- Sebelumnya hanya argc < 4. Awalnya karena argv[4] = /home/plankton/shell.sh maka saya mengira bahwa argc hanya sampai 4 saja, makanya sebelumnya saya menulis argc < 4.

- Ternyata, seperti array, argc dan argv dimulai dari 0. Jadi argv[4] = home/plankton/shell.sh adalah setelah argv[0], argv[1], argv[2], argv[3].

- Sehingga total argc untuk mengeksekusi file mainan adalah 5. Jadi jika kurang dari 5 atau lebih dari 5, maka akan ada error message.

- Sebelumnya program juga tidak di exit setelah error, tetapi sekarang sudah ditambah exit(1) agar program diexit setelah error.
