#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

void downloadUnzip() {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) { // Child process
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
    } else if (pid > 0) { // Parent process
        waitpid(pid, &status, 0); // Menunggu child process selesai
        pid = fork();
        if (pid == 0) { // Child process
            execlp("unzip", "unzip", "-q", "binatang.zip", "-d", "binatang_folder", NULL);
        } else if (pid > 0) { // Parent process
            waitpid(pid, &status, 0); // Menunggu child process selesai
        } else { 
            perror("Error");
            exit(EXIT_FAILURE);
        }
    } else { 
        perror("Error");
        exit(EXIT_FAILURE);
    }
}

void randomImage() {
    DIR *dir;
    struct dirent *entry;
    int count = 0;
    char *ext;
    char path[100];

    if ((dir = opendir("binatang_folder")) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            ext = strrchr(entry->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                count++;
            }
        }

        closedir(dir);
        dir = opendir("binatang_folder");

        srand(time(NULL));
        int r = rand() % count + 1;
        count = 0;
        while ((entry = readdir(dir)) != NULL) {
            ext = strrchr(entry->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                count++;
                if (count == r) {
                    sprintf(path, "%s", entry->d_name);
                    printf("Grape-kun melakukan shift penjagaan pada hewan %s\n", path);
                    break;
                }
            }
        }
        closedir(dir);
    } else {
        perror("Gagal");
    }
}

void makeDirectories() {
    system("mkdir -p HewanDarat HewanAmphibi HewanAir");
}

void move() {
    DIR *dir;
    struct dirent *entry;
    char tempattinggal[256];
    char cmd[256];
    int randomNum;
    srand(time(NULL));

    if ((dir = opendir("binatang_folder")) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                if (strstr(entry->d_name, ".jpg") != NULL || strstr(entry->d_name, ".jpeg") != NULL || strstr(entry->d_name, ".png") != NULL) {
                    // yang mengandung kata darat
                    if (strstr(entry->d_name, "darat") != NULL) {
                        sprintf(tempattinggal, "HewanDarat/");
                    // yang mengandung kata amphibi
                    } else if (strstr(entry->d_name, "amphibi") != NULL) {
                        sprintf(tempattinggal, "HewanAmphibi/");
                    // yang mengandung kata air
                    } else if (strstr(entry->d_name, "air") != NULL) {
                        sprintf(tempattinggal, "HewanAir/");
                    } else {
                        continue;
                    }
                    // Pindah file dari folder binatang_folder ke masing" folder yang sesuai
                    sprintf(cmd, "mv \"binatang_folder/%s\" \"%s%s\"", entry->d_name, tempattinggal, entry->d_name);
                    system(cmd);
                }
            }
        }
        closedir(dir);
    } else {
        perror("Gagal");
    }
}

int main() {
    downloadUnzip();
    randomImage();
    makeDirectories();
    move();

    // zip folder HewanDarat.HewanAmphibi,HewanAir
    pid_t pid;
    int status;

    char* folders[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    int num_folders = 3;

    for (int i = 0; i < num_folders; i++) {
        pid = fork();
        if (pid == 0) { // Child process
            char filename[100];
            sprintf(filename, "%s.zip", folders[i]);
            execlp("zip", "zip", "-r", filename, folders[i], NULL);
        } else if (pid > 0) { // Parent process
            waitpid(pid, &status, 0); // Menunggu child process selesai
        } else { 
            perror("Error");
            exit(EXIT_FAILURE);
        }
    }

    // Hapus folder HewanDarat,HewanAmphibi,HewanAir,binatang_folder
    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    system("rm -r binatang_folder");

    return 0;
}
