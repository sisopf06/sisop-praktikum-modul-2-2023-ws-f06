#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

int main()
{
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
	char dir[256];

    time_t cur_time = time(NULL);
    struct tm* tm = localtime(&cur_time);
    strftime(dir, sizeof(dir), "%Y-%m-%d_%H:%M:%S", tm);
	
    char path[] = "/home/plankton/soalnomor2/";
	strcat(path, dir);

	int child_id = fork();
        if (child_id == -1) {
            perror("fork");
           exit(EXIT_FAILURE);
        }

       if (child_id == 0) {
	    char* argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
        }
        sleep(30);
    }
    exit(EXIT_SUCCESS);
}
