#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    if (argc < 5 || argc > 5) {
        printf("./program (schedule) (/path/ke/sh)\n");
        printf("Contoh: \"./mainan \\* 44 5 /home/plankton/shell.sh\"\n");
        exit(1);
    }

    char *path_sh = argv[4];
    char *input_jam = argv[1];
    char *input_menit = argv[2];
    char *input_detik = argv[3];

    int i = 0;
    int jam, menit, detik;

    if (strcmp(input_menit, "*") == 0) {
        menit = -1;
    } else {
        menit = atoi(input_menit);
        if (menit < 0 || menit > 59) {
            printf("Input Menit tidak valid! (60 < Menit >= 0)\n");
            exit(1);
        }
    }

    if (strcmp(input_jam, "*") == 0) {
        jam = -1;
    } else {
        jam = atoi(input_jam);
        if (jam < 0 || jam > 23) {
            printf("Input Jam tidak valid! (24 > jam >= 0)\n");
            exit(1);
        }
    }

    if (strcmp(input_detik, "*") == 0) {
        detik = -1;
    } else {
        detik = atoi(input_detik);
        if (detik < 0 || detik > 59) {
            printf("Input Detik tidak valid! (60 < Detik >= 0)\n");
            exit(1);
        }
    }

    pid_t pid, sid;
    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        time_t waktu_now;
        struct tm *info;

        time(&waktu_now);
        info = localtime(&waktu_now);

        if ((menit == -1 || menit == info->tm_min)
            &&(jam == -1 || jam == info->tm_hour)
            && (detik == -1 || detik == info->tm_sec)) {

            	pid_t pid2 = fork();

    	        if (pid2 < 0) {
       	            exit(EXIT_FAILURE);
    	        } 
                else if (pid2 == 0) {
                    char *args[] = {"/bin/sh", path_sh, NULL};
                    execv(args[0], args);
                }
	    }
        sleep(1);
	}
    return 0;
}
