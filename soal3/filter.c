#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

void buatTim(int bek, int gelandang, int penyerang){
	pid_t pid = fork();
	if (pid == 0){
		char command[100];
                sprintf(command, "echo \"Kiper:\" >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
		execlp("sh", "sh", "-c", command, NULL);
		exit(1);
	}
	int status;
	wait(&status);

	pid = fork();
	if (pid == 0){
		char command[200];
		sprintf(command,"touch Formasi_%d_%d_%d.txt && ls ./players/Kiper | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang, bek, gelandang, penyerang);
		execlp("sh", "sh", "-c", command, NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "echo \"\\nBek:\" >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "ls -1v ./players/Bek | sort -t _ -k 4n | tail -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "echo \"\\nGelandang:\" >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
		execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid == 0){
                char command[100];
                sprintf(command, "ls -1v ./players/Gelandang | sort -t _ -k 4n | tail -n %d >> ./Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "echo \"\\nPenyerang:\" >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid ==0){
                char command[100];
                sprintf(command, "ls -1v ./players/Penyerang | sort -t _ -k 4n | tail -n %d >> ./Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);
}

int main(){
	char link[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
	pid_t pid = fork();
	if (pid == 0){
		execlp("wget", "wget", "-O", "players.zip", link, NULL);
		exit(1);
	}
	int status;
	wait(&status);

	pid = fork();
	if(pid  == 0){
		execlp("unzip", "unzip", "-q", "players.zip", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
	if(pid == 0){
		execlp("find", "find", "./players", "-type", "f", "!", "-name", "*ManUtd*", "-delete", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
	if(pid == 0){
		execlp("rm", "rm", "players.zip", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
	if(pid == 0){
		execlp("mkdir", "mkdir", "./players/Kiper", "./players/Bek", "./players/Gelandang", "./players/Penyerang", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
	if(pid == 0){
		execlp("find", "find", "./players", "-name", "*Kiper*", "-type", "f", "-exec", "mv", "{}", "./players/Kiper", ";", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
	if(pid == 0){
		execlp("find", "find", "./players", "-name", "*Bek*", "-type", "f", "-exec", "mv", "{}", "./players/Bek", ";", NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
        if(pid == 0){
                execlp("find", "find", "./players", "-name", "*Gelandang*", "-type", "f", "-exec", "mv", "{}", "./players/Gelandang", ";", NULL);
                exit(1);
        }
        wait(&status);

	pid = fork();
        if(pid == 0){
                execlp("find", "find", "./players", "-name", "*Penyerang*", "-type", "f", "-exec", "mv", "{}", "./players/Penyerang", ";", NULL);
                exit(1);
        }
        wait(&status);

	int bek, gelandang, penyerang;
	int sum=1;

	printf("\n\ninput banyak pemain bek yang diinginkan: ");
	scanf("%d", &bek);
	sum += bek;
	printf("%d\n",sum);

	printf("input banyak pemain gelandang yang diinginkan: ");
	scanf("%d", &gelandang);
	sum += gelandang;
	printf("%d\n",sum);

	printf("input banyak pemain penyerang yang diinginkan: ");
	scanf("%d", &penyerang);
	sum += penyerang;
	printf("%d\n",sum);

	if(sum == 11)
		buatTim(bek,gelandang,penyerang);
	else
		printf("Jumlah tim kurang dari standard(11 pemain)\n");
	return 0;
}
